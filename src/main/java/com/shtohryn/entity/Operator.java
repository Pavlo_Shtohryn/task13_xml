package com.shtohryn.entity;

import java.util.Set;
import java.util.HashSet;

public class Operator {
    private String name;
    Set<Tariffs> tariffs = new HashSet<>();
    public Operator(String name, Set<Tariffs> tariffs) {
        this.name = name;
        this.tariffs = tariffs;
    }

    @Override
    public String toString() {
        return "Operator{" +
                "name='" + name + '\'' +
                ", tariffs=" + tariffs +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Tariffs> getTariffs() {
        return tariffs;
    }

    public void setTariffs(Set<Tariffs> tariffs) {
        this.tariffs = tariffs;
    }
}

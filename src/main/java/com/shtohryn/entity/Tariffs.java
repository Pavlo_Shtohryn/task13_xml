package com.shtohryn.entity;

import java.util.Objects;

public class Tariffs {
    private String name;

    public Tariffs(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariffs tariffs = (Tariffs) o;
        return Objects.equals(name, tariffs.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Tariffs{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tariffs() {
    }
}

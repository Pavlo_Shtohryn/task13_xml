package com.shtohryn.entity;

public class CallPrice {
    private int inSameNetwork;
    private int outSameNetwork;
    private int toStationarynetwork;

    @Override
    public String toString() {
        return "CallPrice{" +
                "inSameNetwork=" + inSameNetwork +
                ", outSameNetwork=" + outSameNetwork +
                ", toStationarynetwork=" + toStationarynetwork +
                '}';
    }

    public int getInSameNetwork() {
        return inSameNetwork;
    }

    public void setInSameNetwork(int inSameNetwork) {
        this.inSameNetwork = inSameNetwork;
    }

    public int getOutSameNetwork() {
        return outSameNetwork;
    }

    public void setOutSameNetwork(int outSameNetwork) {
        this.outSameNetwork = outSameNetwork;
    }

    public int getToStationarynetwork() {
        return toStationarynetwork;
    }

    public void setToStationarynetwork(int toStationarynetwork) {
        this.toStationarynetwork = toStationarynetwork;
    }

    public CallPrice(int inSameNetwork, int outSameNetwork, int toStationarynetwork) {
        this.inSameNetwork = inSameNetwork;
        this.outSameNetwork = outSameNetwork;
        this.toStationarynetwork = toStationarynetwork;
    }
}

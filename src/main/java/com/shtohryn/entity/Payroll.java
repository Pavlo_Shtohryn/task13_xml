package com.shtohryn.entity;

public class Payroll {
    private int pricePerMounth;

    @Override
    public String toString() {
        return "Payroll{" +
                "callingPrice=" + pricePerMounth +
                '}';
    }

    public int getCallingPrice() {
        return pricePerMounth;
    }

    public void setCallingPrice(int callingPrice) {
        this.pricePerMounth = callingPrice;
    }

    public Payroll(int callingPrice) {
        this.pricePerMounth = callingPrice;
    }
}

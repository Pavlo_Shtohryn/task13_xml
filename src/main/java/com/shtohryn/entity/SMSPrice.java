package com.shtohryn.entity;

public class SMSPrice {
    private int inSameNetwork;
    private int outSameNetwork;

    @Override
    public String toString() {
        return "SMSPrice{" +
                "inSameNetwork=" + inSameNetwork +
                ", outSameNetwork=" + outSameNetwork +
                '}';
    }

    public int getInSameNetwork() {
        return inSameNetwork;
    }

    public void setInSameNetwork(int inSameNetwork) {
        this.inSameNetwork = inSameNetwork;
    }

    public int getOutSameNetwork() {
        return outSameNetwork;
    }

    public void setOutSameNetwork(int outSameNetwork) {
        this.outSameNetwork = outSameNetwork;
    }

    public SMSPrice(int inSameNetwork, int outSameNetwork) {
        this.inSameNetwork = inSameNetwork;
        this.outSameNetwork = outSameNetwork;
    }
}
